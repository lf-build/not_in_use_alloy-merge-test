FROM node

ENV NGINX_VERSION 1.12.0-1~stretch
ENV NJS_VERSION   1.12.0.0.1.10-1~stretch

RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y git-core
RUN apt-get install -y nginx

WORKDIR /src

RUN npm install -g grunt-cli 
RUN npm install -g bower 
RUN npm install -g git

ADD package.json /src/package.json

RUN npm install

ADD bower.json /src/bower.json
ADD .bowerrc /src/.bowerrc

RUN bower install --allow-root

ADD Gruntfile.js /src/Gruntfile.js
ADD config.js /src/config.js
ADD ./app /src/app

WORKDIR /src

RUN grunt --force build
RUN rm -rf /src/app && rm -rf /src/bower_components && rm -rf /src/node_modules

ADD crossdomain.xml /src/dist/crossdomain.xml
ADD nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

ENTRYPOINT \
    sed -i 's~APP_TOKEN~'${ALLOY_DEFAULT_TOKEN}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~CONFIG_HOST~'${ALLOY_CONFIGURATION_HOST}'~g' /etc/nginx/nginx.conf && \
    sed -i 's~SECURITY_HOST~'${ALLOY_SECURITY_HOST}'~g' /etc/nginx/nginx.conf && \
    find ./dist -type f -exec sed -i 's/CONFIG_PORTAL/'"${ALLOY_PORTAL}"'/g' {} \; && \
    nginx -g 'daemon off;'