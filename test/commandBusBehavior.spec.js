describe("commandBus Behavior", function() {
    var factory = function() {
        return merge(true, commandBus);
    };

    var bus;

    beforeEach(function() {
        bus = factory();
        // publish and subscribe are usually from eventHubBehavior
        bus.subscribe = sinon.spy();
        bus.publish = sinon.spy();
        bus.registry = {};
    });

    it("is defined", function() {
        chai.assert(bus);
    });

    describe("Methods", function() {
        var expectedMethods = [
            "registered",
            "attached",
            "register",
            "dispatch"
        ];

        expectedMethods.forEach(function(method) {
            it(method + " should be defined as a function", function() {
                chai.assert(typeof bus[method] === 'function', "Expected " + method + " to be a function");
            });
        });

        describe("Register", function() {
            it("adds a command and handler to the registry", function() {
                var name = "save";
                var handler = function save() {};

                chai.assert(typeof bus.registry[name] === 'undefined', "Register should be empty");
                bus.register(name, handler);
                chai.assert(bus.registry[name], "Command should be in the registry now");
                chai.assert(typeof bus.registry[name] === 'function', "Command handler should be a function");
            });

            it("ensures that command names and handlers are the correct types", function() {
                chai.assert.throws(function() {
                    var name = 1;
                    var handler = 2;
                    bus.register(name, handler);
                });
            });
        });

        describe("Dispatch", function() {
            it("invokes the correct handler", function() {
                var handler = sinon.spy();
                var name = "save";
                bus.register(name, handler);
                bus.dispatch(name);
                chai.assert(handler.calledOnce, "Handler should have been invoked on calling dispatch");
            });

            it("catches errors thrown by handlers", function(done) {
                var name = "save";
                var handler = function() {
                    throw new Error("An error message");
                };

                bus.register(name, handler);
                bus.dispatch(name)
                    .catch(function(error) {
                        chai.assert.instanceOf(error, Error);
                    })
                    .then(done);
            });

            it("publishes handler errors", function(done) {
                var name = "save";
                var handler = function() {
                    throw new Error("An error message");
                };

                bus.publish = sinon.spy();
                bus.register(name, handler);
                bus.dispatch(name)
                    .catch(function(error) {
                        chai.assert(bus.publish.calledOnce);
                    }).then(done);
            });

            it("publishes after successfully executing a command", function(done) {
                var name = "save";
                var handler = function() {

                };

                bus.register(name, handler);
                bus.dispatch(name)
                    .then(function() {
                        chai.assert.bind(null, bus.publish.calledOnce);
                    })
                    .then(done);
            });
        });
    });

});