// Generated on 2015-08-04 using generator-angular 0.12.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  var serveStatic = require('serve-static');

  grunt.loadNpmTasks('grunt-vulcanize');

  // Automatically load required Grunt tasks
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates',
    cdnify: 'grunt-google-cdn',
    minifyPolymer: 'grunt-minify-polymer',
    replace: 'grunt-text-replace',
    exec: 'grunt-exec'
  });

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'
  };

  // grunt <task> --environment=env
  // ENV VARIABLES
  var environment = grunt.option('environment') || 'dev';
  var portal = grunt.option('portal') || 'LMS';
  var port = grunt.option('port') || 9002;
  var configHost = grunt.option('config') || "";

  // Define the configuration for all the tasks
  grunt.initConfig({

    vulcanize: {
      default: {
        options: {},
        files: {
          "dist/components/vendor.html": "bower_components/paper-tabs/paper-tabs.html"
        }
      }
    },
    // TODO: Fill in dest
    replace: {
      dev: {
        src: ['config.js'],
        dest: 'app/',
        replacements: []
      },
      dist: {
        src: ['config.js'],
        dest: 'dist/',
        replacements: []
      }
    },

    exec: {
      dev: 'sh endpoints.sh dev dist/config.js ' + portal + " " + configHost,
      qa: 'sh endpoints.sh qa dist/config.js ' + portal + " " + configHost,
      demo: 'sh endpoints.sh demo dist/config.js ' + portal + " " + configHost,
      dist: 'sh endpoints.sh dist dist/config.js ' + portal + " " + configHost,
      aws: 'sh endpoints.sh aws dist/config.js ' + portal + " " + configHost,
      clean: 'cp config dist/config.js'
    },

    // Project settings
    yeoman: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: '<%= connect.livereload.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/spec/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma']
      },
      styles: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'autoprefixer']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.livereload.options.livereload %>'
        },
        files: [
          './test/**/*.*',
          '<%= yeoman.app %>/{,*/}*.html',
          '<%= yeoman.app %>/components/**/*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      },
      recess: {
        files: ['<%= yeoman.app %>/styles/{,*/}*.less'],
        tasks: ['recess:inspinia']
      },
      test: {
        files: ['./app/**/*.*', './test/**/*.*'],
        options: {
          livereload: '<%= connect.livereload.options.livereload %>'
        }
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: port,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0'
      },
      livereload: {
        options: {
          livereload: 35729,
          open: true,
          middleware: function (connect) {
            return [
              serveStatic('.tmp'),
              connect().use('/polymer', serveStatic('./bower_components/polymer')),
              connect().use(
                '/bower_components',
                serveStatic('./bower_components')
              ),
              connect().use(
                '/app/styles',
                serveStatic('./app/styles')
              ),
              serveStatic(appConfig.app)
            ];
          }
        }
      },

      // Serve up test runner page
      test: {
        options: {
          port: 9001,
          livereload: 35729,
          open: true,
          middleware: function (connect) {
            return [
              // Serve up the test directory
              serveStatic('test'),
              connect().use('/bower_components', serveStatic('./bower_components')),
              connect().use('/node_modules', serveStatic('./node_modules')),
              connect().use('/app', serveStatic('./app'))
            ];
          }
        }
      },

      dist: {
        options: {
          livereload: false,
          open: true,
          middleware: function (connect) {
            return [
              //   connect().use('/bower_components', serveStatic('./bower_components')),
              //   connect().use('/dist/styles', serveStatic('./dist/styles')),
              connect().use('/polymer', serveStatic('./dist/components/polymer')),
              serveStatic(appConfig.dist)
            ];
          }
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'test/.jshintrc'
        },
        src: ['test/spec/{,*/}*.js']
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= yeoman.dist %>/{,*/}*',
            '!<%= yeoman.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      server: {
        options: {
          map: true,
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Automatically inject Bower components into the app
    // jcuratolo: Should we really be running both app and test subtasks?
    wiredep: {
      app: {
        src: ['<%= yeoman.app %>/{,*/}*.html'],
        ignorePath: /\.\.\//,
        exclude: [
          'bower_components/datatables/media/css/jquery.dataTables.css',
          'polymer.html',
          'polymer-mini.html',
          'polymer-micro.html'
        ],
        options: {
          fileTypes: {
            html: {
              detect: {
                js: /<script.*src=['"]([^'"]+)/gi,
                css: /<link.*href=['"]([^'"]+)/gi,
                html: /<link.*href=['"]([^'"]+)/gi
              },
              replace: {
                js: '<script src="{{filePath}}"></script>',
                css: '<link rel="stylesheet" href="{{filePath}}" />',
                html: '<link rel="import" href="{{filePath}}" />'
              }
            }
          }
        }
      },

      // test: {
      //   devDependencies: true,
      //   src: '<%= karma.unit.configFile %>',
      //   ignorePath: /\.\.\//,
      //   fileTypes: {
      //     js: {
      //       block: /(([\s\t]*)\/{2}\s*?bower:\s*?(\S*))(\n|\r|.)*?(\/{2}\s*endbower)/gi,
      //       detect: {
      //         js: /'(.*\.js)'/gi
      //       },
      //       replace: {
      //         js: '\'{{filePath}}\','
      //       }
      //     }
      //   }
      // }
    },

    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/{,*/}*.js',
          '<%= yeoman.dist %>/styles/{,*/}*.css',
          '<%= yeoman.dist %>/styles/fonts/*'
        ]
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/**/*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      js: ['<%= yeoman.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= yeoman.dist %>',
          '<%= yeoman.dist %>/images',
          '<%= yeoman.dist %>/styles'
        ],
        patterns: {
          js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
        }
      }
    },

    // The following *-min tasks will produce minified files in the dist folder
    // By default, your `index.html`'s <!-- Usemin block --> will take care of
    // minification. These next options are pre-configured if you do not wish
    // to use the Usemin blocks.
    // cssmin: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/styles/main.css': [
    //         '.tmp/styles/{,*/}*.css'
    //       ]
    //     }
    //   }
    // },
    // uglify: {
    //   dist: {
    //     files: {
    //       '<%= yeoman.dist %>/scripts/scripts.js': [
    //         '<%= yeoman.dist %>/scripts/scripts.js'
    //       ]
    //     }
    //   }
    // },
    // concat: {
    //   dist: {}
    // },

    // uglify: {
    //     dist: {
    //         files: {
    //         "<%= yeoman.dist %>/bower_components": ["<%= yeoman.dist %>/bower_components"]
    //         }
    //     }
    // },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= yeoman.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= yeoman.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= yeoman.dist %>',
          src: ['*.html'],
          dest: '<%= yeoman.dist %>'
        }]
      }
    },

    // Replace Google CDN references
    cdnify: {
      dist: {
        html: ['<%= yeoman.dist %>/**/*.html']
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= yeoman.app %>',
          dest: '<%= yeoman.dist %>',
          src: [
            '*.{ico,png,txt}',
            '.htaccess',
            '*.html',
            'images/{,*/}*.{webp}',
            'styles/fonts/{,*/}*.*',
            'components/**/*.*',
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= yeoman.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: 'bower_components/bootstrap/dist',
          src: 'fonts/*',
          dest: '<%= yeoman.dist %>'
        }, {
          expand: true,
          cwd: 'bower_components/fontawesome',
          src: 'fonts/*',
          dest: '<%= yeoman.dist %>'
        },
        {
          expand: true,
          cwd: 'bower_components/polymer',
          dest: '<%= yeoman.dist %>/components/polymer',
          src: ['*.html']
        },
          // {
          //     expand: true,
          //     cwd: 'bower_components',
          //     src: '**/*.*',
          //     dest: '<%= yeoman.dist %>/bower_components'
          // }
        ]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'copy:styles'
      ],
      test: [
        'copy:styles'
      ],
      dist: [
        'copy:styles',
        'imagemin',
        'svgmin'
      ]
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'test/karma.conf.js',
        singleRun: true
      }
    },

    recess: {
      options: {
        compile: true
      },
      inspinia: {
        files: {
          '.tmp/styles/inspinia.css': '<%= yeoman.app %>/styles/inspinia/style.less'
        }
      },
      dist: {
        files: {
          'dist/styles/inspinia.css': '<%= yeoman.app %>/styles/inspinia/style.less'
        }
      }
    },

    minifyPolymer: {
      dist: {
        files: [{
          expand: true,
          cwd: 'bower_components/polymer/',
          src: ['**/*.html'],
          dest: '<%= yeoman.dist %>/components/polymer'
        },
        {
          expand: true,
          cwd: '<%= yeoman.dist %>/components/',
          src: ['**/*.html', '!(polymer)/*.html'],
          dest: '<%= yeoman.dist %>/components/'
        }
        ]
      }
    }

  });

  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([
      'clean:server',
      'wiredep',
      'recess:inspinia',
      'concurrent:server',
      'autoprefixer:server',
      'replace:dev',
      'exec:local',
      'connect:livereload',
      'watch'
    ]);
  });


  grunt.registerTask('dist', 'Run last compiled (dist) version', function (target) {
    grunt.task.run(['connect:dist:keepalive']);
  });

  grunt.registerTask('server', 'DEPRECATED TASK. Use the "serve" task instead', function (target) {
    grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
    grunt.task.run(['serve:' + target]);
  });

  // grunt.registerTask('test', [
  //   'clean:server',
  //   'wiredep',
  //   'concurrent:test',
  //   'autoprefixer',
  //   'connect:test',
  //   'karma'
  // ]);

  grunt.registerTask('build', [
    'clean:dist',
    'replace:dist',
    'wiredep',
    'recess:dist',
    'useminPrepare',
    'concurrent:dist',
    'autoprefixer',
    'concat',
    'copy:dist',
    'cdnify',
    // 'minifyPolymer:dist',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
    'htmlmin',
    'vulcanize'
  ]);

  grunt.registerTask('config', [
    'exec:' + environment
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'test',
    'build'
  ]);

  grunt.registerTask('log', function () {
    console.log("ENVIRONMENT VARIABLE:", environment);
  });

  // Run component tests in browser
  grunt.registerTask('test:browser', [
    'connect:test',
    'watch'
  ]);
};
