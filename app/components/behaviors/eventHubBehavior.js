var eventHub = {
    ready: function () {
        var self = this;
        this.initialize();
    },

    initialize: function () {
        var nameAndOrSourceRequiredError = "Name and/or source are required for subscriptions.";
        var noSubscriptionAddedError = "No subscription added.";
        var handlerTypeError = "Callback must be a function";

        if (!window._eventHub) {
            // Global subscription registry
            var callbacks = [];

            window._eventHub = {
                publish: publish,
                subscribe: subscribe,
                unsubscribe: unsubscribe,
                callbacks: callbacks
            };

            function publish() {
                var event = arguments[0];
                var origin = arguments[2];
                var allArgs = arguments;
                // Strip off event, necessary for inter-op with legacy event system
                var args = Array.prototype.slice.call(arguments, 1);
                // Matches all events
                var WILDCARD = "*";

                // Used to denot event subtypes such as inbox.messages.changed
                var EVENT_SCOPE_DELIMITERS = ":.";

                // Filter callbacks by origin if origin is defined
                function filterByOrigin(origin, callbacks) {
                    if (!origin) {
                        return callbacks;
                    }
                    return callbacks.filter(function (cb) {
                        return cb.context === origin;
                    });
                }

                // Filter callbacks by event if event is defined
                function filterByEvent(event, callbacks) {
                    if (!event) {
                        return callbacks;
                    }

                    return callbacks.filter(function (cb) {
                        // Can subscribe to events with a single wildcard
                        if (WILDCARD === cb.event) {
                            return true;
                        }

                        // Can subscribe to events with only scope delimiters
                        if (EVENT_SCOPE_DELIMITERS.includes(cb.event)) {
                            return true;
                        }

                        // Can subscribe to events with substrings as well
                        return event.toLowerCase().includes(cb.event.toLowerCase());
                    });
                }

                if (typeof event === "undefined") {
                    console.error("Publishing with undefined event.", arguments);
                }

                var filteredCallbacks = filterByOrigin(origin, callbacks);
                filteredCallbacks = filterByEvent(event, filteredCallbacks);

                filteredCallbacks.forEach(function (cb) {
                    if (
                        cb.event === WILDCARD ||
                        cb.event === event ||
                        event.includes(cb.event)
                    ) {
                        // Add event to event payload for visibility in callbacks
                        // Manually published events wont have args defined
                        args[0] = args[0] || {};
                        args[0].event = event;
                        args[0].origin = args[0].origin || origin;
                        cb.handler.apply(cb.context, args);
                    }
                });
            }

            function subscribe(event, handler, context) {
                if (!event && event !== "" && !context) {
                    return console.error(nameAndOrSourceRequiredError, noSubscriptionAddedError);
                }

                if (typeof handler !== 'function') {
                    return console.error(handlerTypeError);
                }

                var call = {
                    event: event,
                    handler: handler,
                    context: context
                };

                callbacks.push(call);

                return call;
            }

            function unsubscribe(subscriptionToRemove) {
                callbacks = callbacks.filter(function (subscription) {
                    return subscription !== subscriptionToRemove;
                });
            }
        }
    },

    publish: function () {
        if (window._eventHub)
            window._eventHub.publish.apply(this, arguments);
    },

    subscribe: function (event, handler, context) {
        if (window._eventHub)
            return window._eventHub.subscribe.call(this, event, handler, context);
    }
}
