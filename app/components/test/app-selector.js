Polymer({
    is: "app-selector",
    properties: {
        appId: {
            type: String
        },
        appLayout: {
            type: String
        },
        appData: {
            type: Object
        }
    },
    // Get Configuration
    // TODO: Create behavior
    alloy: {
        getConfig: function(service, property, callback) {
            this.$.http.get("/configuration/" + service + "/" + property, "//" + window.location.host, callback);
        }
    },
    ready: function() {
        var self = this;
        
        if (window.jQuery) {
            self.init()();
        } else {
            window.addEventListener("WebComponentsReady", self.init());
        }
    },
    init: function() {
        var self = this;
        
        self.appData = self.appData || {};
        window[self.appId] = window[self.appId] || {};
        
        return function() {
            self.getConfiguration();
        }
    },
    // Import Components
    observers: [
        "importComponents(configuration)",
        "getAppData(configuration)"
    ],
    getConfiguration: function() {
        // For local development
        if (document.location.hostname === "localhost") {
            this.configuration = {
                base: "/components/test/",
                // For Root Component
                // isPrivate: true,
                // isAuthorized: true,
                // default: "/components/test/",
                layout: {
                    base: "/components/test/",
                    route: this.appLayout + ".html"
                },
                components: [{
                    base: "/components/test/", 
                    route: this.appId + ".html",
                    position: 'left'
                }],
                requests: [{
                    label: "tenant",
                    action: "GET",
                    base: "http://192.168.1.9:5008",
                    route: "tenant"
                }]
            }                
            return;
        }
        
        // For Production
        // this.configuration = {
        //     components: [{
        //         base: "http://localhost:1337/",
        //         route: "test-app.html",
        //         position: 'left'
        //     }],
        //     layout: {
        //             base: "/components/test/",
        //             route: this.appLayout + ".html"
        //         }
        // }
        // this.configuration = this.alloy.getConfig();
    },
    getAppData: function(configuration) {
        var self = this;
        var requests = configuration.requests;
        
        for (request in requests) {
            var requestData = requests[request];
            if (requestData.action === "GET") {
                self.$.http.get(requestData.route, requestData.base, function(data) {
                    self.appData[requestData.label] = data;
                });
            } else {
                self.$.http[requestData.action](requestData.route,
                requestData.payload,
                requestData.base,
                function(data) {
                    self.appData[requestData.label] = data;
                });
            }
        }
    },
    importComponents: function(configuration) {
        if (!configuration) { return }
        var self = this;
        var components = configuration.components;
        var layout = configuration.layout;
        
        // Load Layout into DOM
        new Promise(function(resolve, reject) {
            self.importHref(layout.base + layout.route, function(e) {
               self.attachComponent(e);
               resolve(); 
            });
        }).then(function() {
            // Load components into layout
            components.forEach(function(component) {
                var href = component.base + component.route;
                
                var host = Polymer.dom(self.$.host);
                var layoutElement = host.childNodes[0];
                var position = Polymer.dom(layoutElement.querySelector("." + component.position));

                self.importHref(href, function(e) {
                    self.attachComponent(e, position)
                });
            });
                
        });
            
    },
    attachComponent: function(e, target) {
        var id = Polymer
            .dom(e.target.import.body)
            .querySelector("dom-module")
            .getAttribute("id");
        var element = document.createElement(id);
        
        target = target || Polymer.dom(this.$.host);

        target.appendChild(element);
        target.flush();
    }
});