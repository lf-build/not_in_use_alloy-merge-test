# Core Components for front-end
## Local development
Run `grunt serve`

## Building and running alloy remotely with docker 
### BUILD ALLOY IMAGE
`docker build -t <image_name> .`

### RUN image with ENV VAR: environment as dist or dev on port 9002 in the background
**Sets configuration endpoint**

Environments set in endpoints.json

`docker run -e environment=<dist_or_dev> -p 9002:9002 -d <image_name>`

##Alloy Core configuration

*If using appRegistry refer to appRegistry README*

**Registry**
```
  "base": "http://localhost:5000/apps/",
  "useRegistry": true,
```

**Local File Structure**
*Most updated version in configuration.json*
```
    "base": "/components/core/"
    "useRegistry": false
```

### Full Configuration
```
{
  "base": "http://localhost:5000/apps/",
  "useRegistry": true,
  "public": {
    "layout": "layouts/public.html",
    "app": "login"
  },
  "private": {
    "layout": "layouts/private.html",
    "app": "dashboard"
  },
  "services": {
    "loanStatusManagement": "http://192.168.1.9:5021",
    "appRegistry": "http://localhost:1337/apps"
  },
  "apps": {
    "app": {
      "src": "file/path.html",
      "configuration": {
        "tabs": [
          "tab1",
          "tab2"
        ]
      }
    }
  }
}
```